var first;
var last;
var age;
var eyeColor;


function Person(first, last, age, eyecolor) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eyecolor;
}


var p1 = new Person("Vinay","Patel",21,"Blue");
var p2 = new Person("Vinay2","Patel2",12,"Blue");
var p3 = new Person("Vinay3","Patel3",23,"Black");
var p4 = new Person("Vinay4","Patel4",26,"Black");
var p5 = new Person("Vinay5","Patel5",27,"Blue");
var p6 = new Person("Vinay6","Patel6",22,"Blue");

var a = [];


a[0]=p1;
a[1]=p2;
a[2]=p3;
a[3]=p4;
a[4]=p5;
a[5]=p6;

// console.log(a[1].age);

for(i=0;i<6;i++){
  console.log(a[i].lastName);
}